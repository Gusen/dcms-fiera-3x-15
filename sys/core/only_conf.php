<?php

		
	# только для тех, у кого уровень доступа больше или равен $level
	function only_level($level=0,$link = NULL)
	{
	global $user;
	if (!isset($user) or $user['level'] < $level)
	{
		if ($link == NULL)$link='/';
		exit(header('Location: '.$link));
	}
	}
	
	
	
	# только для незарегистрированых
	function only_unreg($link = NULL)
	{
	global $user;
	if (isset($user))
	{
		if ($link == NULL)$link='/';
		exit(header('Location: '.$link));
	}
	}
	
	
	# только для зарегистрированых
	function only_reg($link = NULL) 
	{
	global $user;
		if (!isset($user))
		{
			if ($link == NULL) $link = '/';
			$_SESSION['message'] = lang('Только для зарегистрированых');
			exit(header('Location: '.$link));
		}
	}
